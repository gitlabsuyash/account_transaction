from django.apps import AppConfig


class AccountTransactionConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'account_transaction'

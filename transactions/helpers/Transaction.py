import logging
from django.db import transaction
from account_transaction.models import Transaction
from account_transaction.helpers.Account import AccountHelper
from account_transaction.helpers.Exception import InsufficientFunds



class TransactionHelper(AccountHelper):

    def __init__(self, source_account=None, target_account=None, transfer_amount=None):
        super().__init__(source_account=source_account, target_account=target_account, amount=transfer_amount)
        if transfer_amount:
            self.transfer_amount = transfer_amount

    def create_transaction(self, account, transaction_type):
        transaction = Transaction.objects.create(account=account, amount=self.transfer_amount, transaction_type=transaction_type)
        transaction.save()

    def transfer_money(self):
        try:
            with transaction.atomic():
                super().withdraw()
                super().deposit()
                self.create_transaction(super().get_account(self.source_account), 1)
                self.create_transaction(super().get_account(self.target_account),  2)
                self.logger.info("transaction success")
        except InsufficientFunds as e:
            self.logger.info(str(e))

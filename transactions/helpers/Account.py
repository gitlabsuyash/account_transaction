import logging
from account_transaction.models import Accounts
from account_transaction.helpers.Exception import InsufficientFunds


class AccountHelper():

    def __init__(self, source_account=None, target_account=None, amount=None):
        self.logger = logging.getLogger('django')

        if source_account:
            self.source_account =  source_account

        if target_account:
            self.target_account = target_account

        if amount:
            self.amount = amount


    # get account information
    def get_account(self, account_num):
        try:
            account = Accounts.objects.get(account_num=account_num)
        except Accounts.DoesNotExist:
            account = None
            self.logger.info(f'Account {account_num} does not exits.')
        return account


    # update account balance
    def update_balance(self, account_num, balance_amount):
        if account_num:
            account = self.get_account(account_num)
            account.balance = balance_amount
            account.save()
        else:
            self.logger.info(f'Account {account_num} does not exits.')

    def check_for_sufficient_funds(self, source_account, amount):
        if source_account.balance < amount:
            raise InsufficientFunds(source_account.account_num, amount)

    def withdraw(self):
        source_account = self.get_account(self.source_account)
        if source_account:
            self.check_for_sufficient_funds(source_account, self.amount)
            new_balance = source_account.balance - self.amount
            self.update_balance(source_account.account_num, new_balance)

    def deposit(self):
        target_account = self.get_account(self.target_account)
        if target_account:
            amt_deposit = target_account.balance + self.amount
            self.update_balance(target_account.account_num, amt_deposit)
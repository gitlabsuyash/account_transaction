class InsufficientFunds(Exception):
   def __init__(self, account_num=None, amount=None):
       if account_num:
           self.account_num = account_num
       if amount:
           self.amount = amount

   def __str__(self):
        return f"Account {self.account_num} did not have enough funds to transfer {self.amount}"

from django.db import models
from django.contrib.auth.models import User

# Accounts Model.
class Accounts(models.Model):
    SAVING = 'saving'
    RECURRING = 'recurring'
    FIXED_DEPOSIT = 'fixed_deposit'
    ACCOUNT_TYPE = (
        (SAVING, 'saving'),
        (RECURRING, 'recurring'),
        (FIXED_DEPOSIT, 'fixed_deposit'),
    )
    id = models.AutoField(primary_key=True)
    account_num = models.PositiveIntegerField(unique=True)
    account_type = models.CharField(max_length=50, choices=ACCOUNT_TYPE, null=True, blank=True)
    balance = models.DecimalField(decimal_places=2, max_digits=12)
    owner = models.ForeignKey(User, related_name='account_owner', on_delete=models.CASCADE, blank=False, null=False)
    is_active = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'user_accounts'
        ordering = ('-id',)
        indexes = [models.Index(fields=['owner', 'account_num','account_type']) ]


# Transaction Model.
class Transaction(models.Model):
    DEPOSIT = 1
    WITHDRAWAL = 2
    INTEREST = 3
    TRANSACTION_TYPE_CHOICES = (
        (DEPOSIT, 'Deposit'),
        (WITHDRAWAL, 'Withdrawal'),
        (INTEREST, 'Interest'),
    )
    id = models.AutoField(primary_key=True)
    account = models.ForeignKey(Accounts, related_name='transactions',on_delete=models.CASCADE,)
    amount = models.DecimalField(decimal_places=2,max_digits=12)
    transaction_type = models.PositiveSmallIntegerField(choices=TRANSACTION_TYPE_CHOICES)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.account.account_no)

    class Meta:
        db_table = 'transactions'
        ordering = ('created_at',)

